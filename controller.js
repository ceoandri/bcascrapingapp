'use strict';

var fs = require('fs')
var request = require('request')
var cheerio = require('cheerio')
var response = require('./res')
var connection = require('./conn')

exports.index = function(req, resp) {
    let url = 'https://www.bca.co.id/id/Individu/Sarana/Kurs-dan-Suku-Bunga/Kurs-dan-Kalkulator';
    let result = "";
	request(url, function (err, res, html) {
	    if (err && res.statusCode !== 200) throw err;
	    if (!err && res.statusCode == 200) {
	    	const $ = cheerio.load(html);

			var d = new Date();

			var date = d.getDate();
			var month = d.getMonth() + 1;
			var year = d.getFullYear();

			if (month < 10) {
				month = "0" + month;
			}

			var fullDate = year + '-' + month + '-' + date;

	    	connection.query("SELECT count(*) as result FROM kurs where date = " + "'" + fullDate + "'", function (error, rows, fields){
		        if(error){
		            response.error("Tidak ada data baru yang dapat dimasukkan", resp);
		        } else{
		            const tr = $('table.table-bordered tbody tr');
					var json = [];
					var count = parseInt(rows[0].result);
		            if (count === 0) {
				    	tr.each((i, el) => {
				    		var symbol = $(el).find('[align=center]');
				    		var symbolValue = symbol.text().replace(".", "");
				    		symbolValue = symbolValue.replace(",", ".");

				    		var eRateJual = symbol.next();
				    		var eRateJualValue = eRateJual.text().replace(".", "");
				    		eRateJualValue = eRateJualValue.replace(",", ".");

				    		var eRateBeli = eRateJual.next();
				    		var eRateBeliValue = eRateBeli.text().replace(".", "");
				    		eRateBeliValue = eRateBeliValue.replace(",", ".");

				    		var ttCounterJual = eRateBeli.next();
				    		var ttCounterJualValue = ttCounterJual.text().replace(".", "");
				    		ttCounterJualValue = ttCounterJualValue.replace(",", ".");

				    		var ttCounterBeli = ttCounterJual.next();
				    		var ttCounterBeliValue = ttCounterBeli.text().replace(".", "");
				    		ttCounterBeliValue = ttCounterBeliValue.replace(",", ".");

				    		var bankNotesJual = ttCounterBeli.next();
				    		var bankNotesJualValue = bankNotesJual.text().replace(".", "");
				    		bankNotesJualValue = bankNotesJualValue.replace(",", ".");

				    		var bankNotesBeli = bankNotesJual.next();
				    		var bankNotesBeliValue = bankNotesBeli.text().replace(".", "");
				    		bankNotesBeliValue = bankNotesBeliValue.replace(",", ".");

				    		var insertQuery = "INSERT INTO kurs (symbol, e_rate_jual, e_rate_beli, tt_counter_jual, tt_counter_beli, bank_notes_jual, bank_notes_beli, date) VALUES (";
					    		insertQuery += "'" + symbolValue + "'" + ", ";
					    		insertQuery += eRateJualValue + ", ";
					    		insertQuery += eRateBeliValue + ", ";
					    		insertQuery += ttCounterJualValue + ", ";
					    		insertQuery += ttCounterBeliValue + ", ";
					    		insertQuery += bankNotesJualValue + ", ";
					    		insertQuery += bankNotesBeliValue + ", ";
					    		insertQuery += "'" + fullDate + "'" + ")";
				    		
				    		connection.query(insertQuery, function (error, rows, fields){
						        if(error){
						            console.log(error)
						        }
						    });

						    var itemJson = { symbol: "", e_rate: { jual: "", beli: "" }, tt_counter: { jual: "", beli: "" }, bank_notes: { jual: "", beli: "" }, date: "" }

						  	itemJson.symbol = symbolValue;
						  	itemJson.e_rate.jual = eRateJualValue;
						  	itemJson.e_rate.beli = eRateBeliValue;
						  	itemJson.tt_counter.jual = ttCounterJualValue;
						  	itemJson.tt_counter.beli = ttCounterBeliValue;
						  	itemJson.bank_notes.jual = bankNotesJualValue;
						  	itemJson.bank_notes.beli = bankNotesBeliValue;
						  	itemJson.date = fullDate;

						  	json.push(itemJson)
				    	});	
				    	response.ok(json, resp);
				    } else {
				    	response.ok("Tidak ada data baru yang dapat dimasukkan", resp);
				    }
		        }
		    });
	    }
	});
};

exports.getKurs = function(req, res) {
	var getQuery = "SELECT * FROM kurs where date >= '" + req.query.startdate + "' AND date <= '" + req.query.enddate + "'";
    connection.query(getQuery, function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
        	var json = []
        	rows.forEach(function(item, index, array) {
			  	var itemJson = { symbol: "", e_rate: { jual: "", beli: "" }, tt_counter: { jual: "", beli: "" }, bank_notes: { jual: "", beli: "" }, date: "" }

			  	var symbol = item.symbol;
			  	var eRateJual = item.e_rate_jual;
			  	var eRateBeli = item.e_rate_beli;
			  	var ttCounterJual = item.tt_counter_jual;
			  	var ttCounterBeli = item.tt_counter_beli;
			  	var bankNotesJual = item.bank_notes_jual;
			  	var bankNotesBeli = item.bank_notes_beli;
			  	var date = item.date;

			  	itemJson.symbol = symbol;
			  	itemJson.e_rate.jual = eRateJual;
			  	itemJson.e_rate.beli = eRateBeli;
			  	itemJson.tt_counter.jual = ttCounterJual;
			  	itemJson.tt_counter.beli = ttCounterBeli;
			  	itemJson.bank_notes.jual = bankNotesJual;
			  	itemJson.bank_notes.beli = bankNotesBeli;
			  	itemJson.date = date;

			  	json.push(itemJson)
			});
            response.ok(json, res)
        }
    });
};

exports.getKursByCurrency = function(req, res) {
	let symbol = req.params.symbol;
	var getQuery = "SELECT * FROM kurs where symbol = '" + symbol + "' AND date >= '" + req.query.startdate + "' AND date <= '" + req.query.enddate + "'";
    connection.query(getQuery, function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            var json = []
        	rows.forEach(function(item, index, array) {
			  	var itemJson = { symbol: "", e_rate: { jual: "", beli: "" }, tt_counter: { jual: "", beli: "" }, bank_notes: { jual: "", beli: "" }, date: "" }

			  	var symbol = item.symbol;
			  	var eRateJual = item.e_rate_jual;
			  	var eRateBeli = item.e_rate_beli;
			  	var ttCounterJual = item.tt_counter_jual;
			  	var ttCounterBeli = item.tt_counter_beli;
			  	var bankNotesJual = item.bank_notes_jual;
			  	var bankNotesBeli = item.bank_notes_beli;
			  	var date = item.date;

			  	itemJson.symbol = symbol;
			  	itemJson.e_rate.jual = eRateJual;
			  	itemJson.e_rate.beli = eRateBeli;
			  	itemJson.tt_counter.jual = ttCounterJual;
			  	itemJson.tt_counter.beli = ttCounterBeli;
			  	itemJson.bank_notes.jual = bankNotesJual;
			  	itemJson.bank_notes.beli = bankNotesBeli;
			  	itemJson.date = date;

			  	json.push(itemJson)
			});
            response.ok(json, res)
        }
    });
};

exports.insertKurs = function(req, res) {
	var symbol = req.body.symbol;
	var eRateJual = req.body.e_rate.jual;
	var eRateBeli = req.body.e_rate.beli;
	var ttCounterJual = req.body.tt_counter.jual;
	var ttCounterBeli = req.body.tt_counter.beli;
	var bankNotesJual = req.body.bank_notes.jual;
	var bankNotesBeli = req.body.bank_notes.beli;
	var date = req.body.date;

	connection.query("SELECT count(*) as result FROM kurs where date = " + "'" + date + "' and symbol = '" + symbol + "'", function (error, rows, fields){
        var count = parseInt(rows[0].result);
	    if (count === 0) {
	    	var insertQuery = "INSERT INTO kurs (symbol, e_rate_jual, e_rate_beli, tt_counter_jual, tt_counter_beli, bank_notes_jual, bank_notes_beli, date) VALUES (";
				insertQuery += "'" + symbol + "'" + ", ";
				insertQuery += eRateJual + ", ";
				insertQuery += eRateBeli + ", ";
				insertQuery += ttCounterJual + ", ";
				insertQuery += ttCounterBeli + ", ";
				insertQuery += bankNotesJual + ", ";
				insertQuery += bankNotesBeli + ", ";
				insertQuery += "'" + date + "'" + ")";
			connection.query(insertQuery, function (error, rows, fields){
		        if(error){
		            response.error(error, res)
		        } else{
		        	var json = [];
		        	json.push(req.body)
		            response.ok(json, res)
		        }
		    });
	    } else {
	    	response.error("Data sudah ada, silahkan masukkan data yang lain", res);
	    }
    });		
};

exports.updateKurs = function(req, res) {
	var symbol = req.body.symbol;
	var eRateJual = req.body.e_rate.jual;
	var eRateBeli = req.body.e_rate.beli;
	var ttCounterJual = req.body.tt_counter.jual;
	var ttCounterBeli = req.body.tt_counter.beli;
	var bankNotesJual = req.body.bank_notes.jual;
	var bankNotesBeli = req.body.bank_notes.beli;
	var date = req.body.date;

	connection.query("SELECT count(*) as result FROM kurs where date = " + "'" + date + "' and symbol = '" + symbol + "'", function (error, rows, fields){
        var count = parseInt(rows[0].result);
	    if (count === 0) {
	    	var updateQuery = "UPDATE kurs SET symbol = '";
				updateQuery += symbol + "', e_rate_jual = ";
				updateQuery += eRateJual + ", e_rate_beli = "; 
				updateQuery += eRateBeli + ", tt_counter_jual = ";
				updateQuery += ttCounterJual + ", tt_counter_beli = "; 
				updateQuery += ttCounterBeli + ", bank_notes_jual = "; 
				updateQuery += bankNotesJual + ", bank_notes_beli = "; 
				updateQuery += bankNotesBeli + ", date = '" + date + "' ";
				updateQuery += "where symbol = '" + symbol + "' ";
				updateQuery += "and date = '" + date + "'";
			connection.query(updateQuery, function (error, rows, fields){
		        if(error){
		            response.error(error, res)
		        } else{
		        	var json = [];
		        	json.push(req.body)
		            response.ok(json, res)
		        }
		    });
	    } else {
	    	response.error("Data sudah ada, silahkan masukkan data yang lain", res);
	    }
    });
};

exports.deleteKurs = function(req, res) {
	let date = req.params.date;
	var deleteQuery = "DELETE FROM kurs where date = " + "'" + date + "'";

	connection.query("SELECT count(*) as result FROM kurs where date = " + "'" + date + "'", function (error, rows, fields){
        var count = parseInt(rows[0].result);
	    if (count === 0) {
	    	response.error("Data tidak tersedia", res);
	    } else {
			connection.query(deleteQuery, function (error, rows, fields){
		        if(error){
		            response.error(error, res)
		        } else{
		            response.ok("Data berhasil di hapus", res)
		        }
		    });
	    }
    });

};