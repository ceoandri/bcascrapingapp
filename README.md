# BCA Scraping App

BCA Scraping App is a sample of RESTful API.

## Installation

Use the package manager [npm](https://www.npmjs.com/get-npm) to install BCA Scraping App.

```bash
clone the project
```

```bash
cd bcascrapingapp
```

```bash
npm install
```

```bash
create your mysql database //you can name it as kurs_database or anything that you want
create table kurs //you can import the template from kurs.sql
```

```bash
create .env file //you can use the template from .env.example
fill the information about your mysql database
```

## Run the App

Use the [node](https://nodejs.org/en/) to run BCA Scraping App.

```bash
node index.js
```

## Unit testing

Use the package manager [npm](https://www.npmjs.com/get-npm) to test BCA Scraping App.

```bash
npm test
```

## Test Api using Postman

Use the package manager [npm](https://www.npmjs.com/get-npm) to test BCA Scraping App using Postman.

```bash
import the BCA Scraping App Routing.postman_collection.json in Postman app.

Now you can run the request :)
```

## License
[MIT](https://choosealicense.com/licenses/mit/)